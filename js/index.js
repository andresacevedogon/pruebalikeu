/**
 * @name getAllPersonajes
 * @desc Funcion que se carga al iniciar la pagina, carga los datos del API y hace una iteración para pintar la vista
 */
function getAllPersonajes() {
    impPersonajes = [];
    // se conecta al api
    axios.get('https://rickandmortyapi.com/api/character/')
        .then(function (res) {
            // se itera la respuesta y se pasa a HTML
            for (let i = 0; i < res.data.results.length; i++) {
                this.impPersonajes.push(
                    '<div class=\'col-12 col-sm-6 col-lg-3 margin-0 text-center  bg-oscuro\'>' +
                    '<div class="card text-white bg-dark" style="margin-top: 10px;">' +
                    '            <img class=\'img-personajes\' src="' + res.data.results[i].image + '"/>' +
                    // '              <a href="javascript:getPersonaje(' + res.data.results[i].id + ')" class="card-link">Ver mas</a>' +
                    '              <a href="personaje.html?id=' + res.data.results[i].id + '" class="card-link">Ver mas</a>' +
                    '            <div class="card-body">' +
                    '              <h5 class="card-title">' + res.data.results[i].name + '</h5>' +
                    '              <p class="card-text">' + res.data.results[i].origin.name + '</p>' +
                    '            </div>' +
                    '<table className="table">' +
                    '<thead>' +
                    '<tr>' +
                    '<th scope="col">Estado</th>' +
                    '<th scope="col">Genero</th>' +
                    '<th scope="col">Especie</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '<tr>' +
                    '<th scope="row">' + res.data.results[i].status + '</th>' +
                    '<th scope="row">' + res.data.results[i].gender + '</th>' +
                    '<th scope="row">' + res.data.results[i].species + '</th>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '            <div class="card-body">' +
                    '            </div>' +
                    '          </div>' +
                    '          </div>');
            }
            // se pinta el HTML
            document.getElementById('tarjeta').innerHTML = this.impPersonajes.join(" ");
        })
        .catch(function (error) {
            console.log(error);
        })
        .then(function () {
        });
}

/**
 * @name gerPersonaje
 * @desc Funcion que se carga al dar click en un personaje para renderizar el html
 */
function gerPersonaje() {
    //Se captura la url de la pagina
    let loc = document.location.href;
    // se saca el id para realizar la busqueda
    let getId = loc.split('?id=')[1];
    // se conecta al api y se pasa el id
    axios.get('https://rickandmortyapi.com/api/character/' + getId)
        .then(function (res) {
            dataPersonaje = res.data;
            document.getElementById('imagen').innerHTML = ' <img src="' + dataPersonaje.image + '" class="card-img">';
            document.getElementById('nombre').innerHTML = dataPersonaje.name;
            document.getElementById('estado').innerHTML = dataPersonaje.status;
            document.getElementById('especie').innerHTML = dataPersonaje.species;
            document.getElementById('tipo').innerHTML = dataPersonaje.type;
            document.getElementById('genero').innerHTML = dataPersonaje.gender;
            document.getElementById('origen').innerHTML = dataPersonaje.origin.name;
            document.getElementById('locacion').innerHTML = dataPersonaje.location.name;

            episodios = [];
            for (let i = 0; i < dataPersonaje.episode.length; i++) {
                this.episodios.push(
                    '<a href="' + dataPersonaje.episode[i] + '"class="card-link">Episodio: ' + i + '</a>');
            }
            // se pinta el HTML
            document.getElementById('episodio').innerHTML = this.episodios.join(" ");
        })
        .catch(function (error) {
            console.log(error);
        })
        .then(function () {
        });
}
